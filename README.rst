==============================
SWIM-PDE
==============================

``SWIM-PDE`` is a framework for solving partial differential equations using neural networks that was presented in [1]_ (`arxiv`_ pre-print). The solvers approximate the solution by a linear combination of basis functions, that constitue a shallow neural network. The weights and biases are then chosen to fit the PDE in a discrete set of points in the domain. The weights of the neural networks are sampled based on the SWIM algorithm (see [2]_ for more details). This speeds up the fitting procedure, as no iterative optimization algorithm is required.

Installation
------------

To install the main package with the requirements, one needs to clone the repository and execute the following command from the root folder:

.. code-block:: bash

    pip install .
    
You may need to install the `swimnetworks`_ module first.

Examples
--------

Folder ``examples`` constains Jupyter notebook showing solver for several forward and inverse problems. Note that you may need to install additional dependencies to run the examples.
The numerical experiments from [1]_ can be found in a separate `repository`_.

Citation
--------

If you use the SWIM-PDE package in your research, please cite the following papers:

.. [1] C\. Datar, T. Kapoor, A. Chandra, Q. Sun, I. Burak, E. Bolager, A. Veselovska, M. Fornasier, F. Dietrich. Solving partial differential equations with sampled neural networks, pre-print, 2024; arxiv:2405.20836.
.. [2] E\. Bolager, I. Burak, C. Datar, Q. Sun, F. Dietrich. Sampling weights of deep neural networks. NeurIPS 2023; arXiv:2306.16830.

.. _repository: https://gitlab.com/felix.dietrich/swimpde-paper
.. _swimnetworks: https://gitlab.com/felix.dietrich/swimnetworks
.. _arxiv: http://arxiv.org/abs/2405.20836