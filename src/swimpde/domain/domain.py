from dataclasses import dataclass
from typing import Callable

import numpy as np
import numpy.typing as npt
from scipy.integrate import cumulative_trapezoid as cumtrapz
from scipy.interpolate import interp1d


@dataclass(kw_only=True)
class Domain:
    """
    Specifies a domain as a collection of points.

    Attributes
    ----------
    interior_points: npt.ArrayLike
        Points in the interior of the domain.
    boundary_points: npt.ArrayLike
        Points in the boundary of the domain.
    normal_vectors: npt.ArrayLike
        Normal vectors at the boundary points of the domain.
    coupled: bool
        If True, treats the last dimension of the domain as time.
    all_points: npt.ArrayLike
        Interior and boundary points stecked together.


    Methods
    -------
    get_core_interior_mask(self, margin: float) -> npt.ArrayLike
        Returns a mask of the interior points that are not near the boundary.
    """

    interior_points: npt.ArrayLike = None
    boundary_points: npt.ArrayLike = None
    normal_vectors: npt.ArrayLike = None
    _domain_box: npt.ArrayLike = None
    n_dim: int = None
    coupled: bool = False

    def __post_init__(self):
        self.n_dim = self.interior_points.shape[1]

        if self.boundary_points is None:
            self.boundary_points = np.empty((0, self.n_dim))

        if self.normal_vectors is not None:
            norm = np.linalg.norm(self.normal_vectors, axis=-1, keepdims=True)
            self.normal_vectors = self.normal_vectors / norm

        if self._domain_box is None:
            ax_mins = np.min(self.all_points, axis=0)
            ax_maxs = np.max(self.all_points, axis=0)
            self._domain_box = np.row_stack([ax_mins, ax_maxs])

    def get_core_interior_mask(self, margin: float) -> npt.ArrayLike:
        """Returns a mask of the interior points that are not near the boundary.

        The threshold distance is computed as margin*domain_size, where the
        domain size is estimated by a box containing all interior points.
        """
        if margin == 0:
            return np.full(self.interior_points.shape[0], True)

        ax_mins, ax_maxs = self._domain_box
        domain_size = np.linalg.norm(ax_maxs - ax_mins)
        threshold = domain_size * margin
        diffs = self.interior_points[:, None, :] - self.boundary_points[None, :, :]
        distances = np.linalg.norm(diffs, axis=-1)
        min_distances = np.min(distances, axis=-1)
        return min_distances > threshold

    @property
    def all_points(self) -> npt.ArrayLike:
        return np.row_stack([self.boundary_points, self.interior_points])


@dataclass(kw_only=True)
class ResamplingDomain(Domain):
    resampling_pdf: Callable
    random_seed: int
    sample_points: npt.ArrayLike = None

    def __post_init__(self):
        super().__post_init__()

        if self.n_dim != 1:
            raise ValueError("ResamplingDomain is only implemented for 1D domains.")

        if self.sample_points is None:
            self.sample_points = self.interior_points
        # The sampling points need to be sorted for a proper
        # interpolation of the CDF.
        self.sample_points = np.sort(self.sample_points, axis=0)

    def resample_domain(self, gradient_fn: Callable[[npt.ArrayLike], npt.ArrayLike]):
        rng = np.random.default_rng(seed=self.random_seed)

        # The data is 1D, so we have only one coordinate for every gradient.
        # The functions is 1D, so the gradients are simply vectors.
        samples = self.sample_points[:, 0]
        gradients = gradient_fn(samples[:, None])[:, 0, 0]

        # Compute new probability over the domain.
        pdf = self.resampling_pdf(gradients)
        cdf = cumtrapz(pdf, samples, initial=0)
        cdf /= cdf[-1]  # Normalize to make sure it goes from 0 to 1
        # Create an interpolation function for the inverse CDF
        inv_cdf_interp = interp1d(cdf, samples, kind="quadratic", bounds_error=True)
        # Generate random samples from a uniform distribution
        n_resampled = self.interior_points.shape[0]
        uniform_samples = rng.uniform(size=n_resampled)
        # Use the inverse CDF interpolation function to generate new interior points
        self.interior_points = inv_cdf_interp(uniform_samples)[:, None]
