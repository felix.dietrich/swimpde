from .static_solver import StaticSolver
from .time_solver import TimeDependentSolver

__all__ = ["StaticSolver", "TimeDependentSolver"]
