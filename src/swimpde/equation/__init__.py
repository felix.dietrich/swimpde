from .advection import Advection
from .burgers import Burgers
from .euler_bernoulli import EulerBernoulli
from .helmholtz import Helmholtz
from .poisson import Poisson

__all__ = [
    "Advection",
    "Burgers",
    "Helmholtz",
    "EulerBernoulli",
    "Poisson",
]
