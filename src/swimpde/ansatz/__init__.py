from .basic_ansatz import BasicAnsatz
from .boundary_compliant_ansatz import BoundaryCompliantAnsatz

__all__ = ["BoundaryCompliantAnsatz", "BasicAnsatz"]
